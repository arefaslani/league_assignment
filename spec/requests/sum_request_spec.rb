require 'spec_helper'

RSpec.describe 'POST /sum' do
  include_examples 'feature spec shared examples',
                   '/sum',
                   '45'
end
