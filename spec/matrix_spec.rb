require 'spec_helper'
require_relative '../lib/league_assignment/matrix'

RSpec.describe LeagueAssignment::Matrix do
  let(:rows) do
    [
      [1, 2, 3],
      [4, 5, 6],
      [7, 8, 9]
    ]
  end

  describe '.build' do
    subject { described_class.build(matrix_string) }

    let(:matrix_string) { "1,2,3\n4,5,6\n7,8,9" }

    it { is_expected.to be_instance_of described_class }

    describe 'with invalid matrix string' do
      let(:matrix_string) { "1,2,3\n4,5,6,7\n8,9" }

      it 'is expected to raise LeagueAssignment::InvalidMatrix' do
        expect{ subject }.to raise_error LeagueAssignment::InvalidMatrix
      end
    end

    describe '#rows' do
      subject { super().rows }

      it { is_expected.to eq rows }

      context 'with a trailing new line character' do
        let(:matrix_string) { "1,2,3\n4,5,6\n7,8,9\n" }

        it { is_expected.to eq rows }
      end
    end

  end

  describe '#echo' do
    subject { described_class.new(rows).echo }

    it { is_expected.to eq "1,2,3\n4,5,6\n7,8,9" }

    context 'with an empty matrix' do
      let(:rows) { [] }

      it { is_expected.to eq '' }
    end
  end

  describe '#invert' do
    subject { described_class.new(rows).invert }

    it { is_expected.to eq "1,4,7\n2,5,8\n3,6,9" }

    context 'with an empty matrix' do
      let(:rows) { [] }

      it { is_expected.to eq '' }
    end
  end

  describe '#flatten' do
    subject { described_class.new(rows).flatten }

    it { is_expected.to eq '1,2,3,4,5,6,7,8,9' }

    context 'with an empty matrix' do
      let(:rows) { [] }

      it { is_expected.to eq '' }
    end
  end

  describe '#sum' do
    subject { described_class.new(rows).sum }

    it { is_expected.to eq 45 }

    context 'with an empty matrix' do
      let(:rows) { [] }

      it { is_expected.to eq 0 }
    end
  end

  describe '#multiply' do
    subject { described_class.new(rows).multiply }

    it { is_expected.to eq 362880 }

    context 'with an empty matrix' do
      let(:rows) { [] }

      it { is_expected.to eq 0 }
    end
  end
end
