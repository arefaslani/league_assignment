ARG RUBY_VERSION

FROM ruby:${RUBY_VERSION}-alpine3.15 as base

ARG APP_HOME=/league

WORKDIR ${APP_HOME}

RUN mkdir -p vendor/bundle

RUN apk add --update build-base

ARG BUNDLER_VERSION
RUN gem install bundler:${BUNDLER_VERSION} && \
    bundle config path vendor/bundle

FROM base as build

COPY .ruby-version Gemfile Gemfile.lock ./
COPY --from=base ${APP_HOME}/vendor vendor

RUN bundle install --jobs 4 --retry 3

COPY . .

FROM ruby:${RUBY_VERSION}-alpine3.15 as runtime

ARG BUNDLER_VERSION

RUN gem install bundler:${BUNDLER_VERSION} && \
  bundle config path vendor/bundle

ARG APP_HOME=/league

WORKDIR $APP_HOME

COPY --from=build ${APP_HOME} .

EXPOSE 3000

CMD ["bundle", "exec", "rackup", "-p", "3000", "--host", "0.0.0.0"]
