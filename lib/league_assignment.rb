# frozen_string_literal: true

require 'roda'
require 'ostruct'
require_relative './league_assignment/matrix'

module LeagueAssignment
  class NoFileUpload < RuntimeError; end
  class InvalidFileFormat < RuntimeError; end

  class App < Roda
    route do |r|
      r.post 'echo' do
        matrix.echo
      end

      r.post 'invert' do
        matrix.invert
      end

      r.post 'flatten' do
        matrix.flatten
      end

      r.post 'sum' do
        matrix.sum.to_s
      end

      r.post 'multiply' do
        matrix.multiply.to_s
      end
    end

    plugin :error_handler do |e|
      case e.class.to_s
      when 'LeagueAssignment::NoFileUpload',
           'LeagueAssignment::InvalidFileFormat'
        response.status = 400
      when 'LeagueAssignment::InvalidMatrix'
        response.status = 422
      end

      e.message
    end

    plugin :not_found do
      response.status = 404
      "Huh?"
    end

    private

    def uploaded_csv
      unless file_param.filename.match?(/\.csv$/)
        raise InvalidFileFormat, 'Invalid file format'
      end

      file_param.tempfile.read
    end

    def file_param
      raise NoFileUpload, 'No file uploaded' unless request.params['file']

      return OpenStruct.new(request.params['file'])
    end

    def matrix
      LeagueAssignment::Matrix.build(uploaded_csv)
    end
  end
end
