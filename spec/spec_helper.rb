# frozen_string_literal: true

require 'rack/test'
require "league_assignment"

def app
  LeagueAssignment::App.freeze.app
end

def fixture_path(name)
  File.join(File.dirname(__FILE__), 'fixtures', name)
end

Dir[File.join(File.dirname(__FILE__), 'support/**/*.rb')].
  each { |f| require f }

RSpec.configure do |config|
  # Enable Rack helper methods
  config.include Rack::Test::Methods

  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end
