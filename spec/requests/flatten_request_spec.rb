require 'spec_helper'

RSpec.describe 'POST /flatten' do
  include_examples 'feature spec shared examples',
                   '/flatten',
                   '1,2,3,4,5,6,7,8,9'
end
