# frozen_string_literal: true
module LeagueAssignment
  class InvalidMatrix < RuntimeError; end

  # Provides methods to do operations on a matrix
  class Matrix
    attr_reader :rows

    # Builds a Matrix object from an string representation of a square matrix.
    #
    # @param format [String] a string representation of the matrix
    # @return [Matrix] a square Matrix.
    #
    # @example
    #   LeagueAssignment::Matrix.build("1,2\n3,4")
    #      #=> #<LeagueAssignment::Matrix:0x00000001130a0eb0 @rows=[[1, 2], [3, 4]]>
    # @example
    #   LeagueAssignment::Matrix.build("1,2\n3,4,5") #=> LeagueAssignment::InvalidMatrix
    #
    def self.build(matrix_string)
      rows = matrix_string.strip.split("\n")
      outcome = rows.map do |row|
        columns = row.split(',').map(&:to_i)

        raise InvalidMatrix, 'Invalid matrix' if columns.length != rows.length

        columns
      end

      new outcome
    end

    def initialize(rows = [])
      @rows = rows
    end

    # Returns the matrix as a string in matrix format.
    #
    # @return [String] a string representation of the matrix
    #
    # @example
    #   matrix.echo #=> "1,2,3\n3,4,5\n7,8,9"
    #
    def echo
      rows.map do |row|
        row.join(',')
      end.join("\n")
    end

    # Returns the matrix as a string in matrix format where the columns and rows
    # are inverted
    #
    # @return [String] a string representation of the inverted matrix
    #
    # @example
    #   matrix.invert #=> "1,4,7\n2,5,8\n3,6,9"
    #
    def invert
      rows.length.times.map do |i|
        rows.map do |row|
          row[i]
        end.join(',')
      end.join("\n")
    end

    # Returns the matrix as a 1 line string, with values separated by commas.
    #
    # @return [String] a string representation of the flattened matrix
    #
    # @example
    #   matrix.flatten #=> "1,2,3,4,5,6,7,8,9"
    #
    def flatten
      # It is also possible to use Ruby's Array#flatten method
      rows.map do |row|
        row.join(',')
      end.join(',')
    end

    # Returns the sum of the integers in the matrix.
    #
    # @return [Integer] sum of all items in the matrix
    #
    # @example
    #   matrix.sum #=> 45
    #
    def sum
      return 0 if rows.length == 0

      rows.map do |row|
        row.sum
      end.sum
    end

    # Returns the product of the integers in the matrix.
    #
    # @return [Integer] product of all items in the matrix
    #
    # @example
    #   matrix.sum #=> 362880
    #
    def multiply
      return 0 if rows.length == 0

      rows.map do |row|
        row.reduce(1) { |index, acc| acc *= index }
      end.reduce(1) { |index, acc| acc *= index }
    end
  end
end
