require 'spec_helper'

RSpec.describe 'POST /echo' do
  include_examples 'feature spec shared examples',
                   '/echo',
                   "1,2,3\n4,5,6\n7,8,9"
end
