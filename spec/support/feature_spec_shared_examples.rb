# frozen_string_literal: true
RSpec.shared_examples 'feature spec shared examples' do |endpoint, successful_body|
  before do
    post endpoint, {
      'file' => Rack::Test::UploadedFile.new(
        fixture_path('sample_matrix.csv'),
        'multipart/form-data'
      )
    }
  end

  it 'responds with ok' do
    expect(last_response.status).to eq 200
  end

  it 'returns a valid response body' do
    expect(last_response.body).to include successful_body
  end

  context 'when the file param is ignored' do
    before do
      post endpoint
    end

    it 'responds with bad request' do
      expect(last_response.status).to eq 400
      expect(last_response.body).to eq 'No file uploaded'
    end
  end

  context 'when the file format is invalid' do
    before do
      post endpoint, {
        'file' => Rack::Test::UploadedFile.new(
          fixture_path('invalid_format.txt'),
          'multipart/form-data'
        )
      }
    end

    it 'responds with bad request' do
      expect(last_response.status).to eq 400
      expect(last_response.body).to eq 'Invalid file format'
    end
  end

  context 'when the input matrix is invalid' do
    before do
      post endpoint, {
        'file' => Rack::Test::UploadedFile.new(
          fixture_path('invalid_matrix.csv'),
          'multipart/form-data'
        )
      }
    end

    it 'responds with bad request' do
      expect(last_response.status).to eq 422
      expect(last_response.body).to eq 'Invalid matrix'
    end
  end
end
