require 'spec_helper'

RSpec.describe 'POST /invert' do
  include_examples 'feature spec shared examples',
                   '/invert',
                   "1,4,7\n2,5,8\n3,6,9"
end
