RUBY_VERSION = $(shell cat .ruby-version)
BUNDLER_VERSION = $(shell grep -A1 'BUNDLED WITH' Gemfile.lock |tail -1 |xargs)

build:
	@DOCKER_BUILDKIT=1 docker build -t league_assignment \
	--build-arg RUBY_VERSION=$(RUBY_VERSION) \
	--build-arg BUNDLER_VERSION=$(BUNDLER_VERSION) \
	--target runtime \
	.

test:
	@DOCKER_BUILDKIT=1 docker run league_assignment bundle exec rspec

dev:
	@DOCKER_BUILDKIT=1 docker run -p 3000:3000 league_assignment
