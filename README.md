# LeagueAssignment
A simple Ruby webserver that provides endpoints to do operation on a
square matrix passed through HTTP POST using a csv file.

__*NOTE*__: Since the setup is based on Docker, make sure you have Docker
installed before continue.

## Setup
Run `make build`.

## Running test
Run `make test`.

## Running the webserver
Run `make dev`.

## Sending example requests
After running the webserver, you can send requests to test the operations on the
specified matrix. The request should send a `file` parameter in the body of the
request with `multipart/form-data` mime type. Here are some examples of valid
requests:
```
curl -F 'file=@./spec/fixtures/sample_matrix.csv' "localhost:3000/echo"
curl -F 'file=@./spec/fixtures/sample_matrix.csv' "localhost:3000/invert"
curl -F 'file=@./spec/fixtures/sample_matrix.csv' "localhost:3000/flatten"
curl -F 'file=@./spec/fixtures/sample_matrix.csv' "localhost:3000/sum"
curl -F 'file=@./spec/fixtures/sample_matrix.csv' "localhost:3000/multiply"
```
And here are some examples of invalid requests:
1. Request without a file parameter:
```
curl -X POST "localhost:3000/echo"
```
2. Request with a wrong file format:
```
curl -F 'file=@./spec/fixtures/invalid_format.txt' "localhost:3000/echo"
```
3. Request with an invalid matrix:
```
curl -F 'file=@./spec/fixtures/invalid_matrix.csv' "localhost:3000/echo"
```
